package com.capgemini.osce.quaff.slurepernetes.kubernetes

import io.fabric8.kubernetes.api.model.Pod
import org.springframework.stereotype.Component

@Component
class StatusExtractor {

    companion object {
        private val READY_MAPPING = mapOf(
                true to "green",
                false to "red"
        )
    }

    fun toSimpleStatus(pod: Pod): Map<String,String> = mapOf(
            "id" to pod.metadata.name,
            "status" to READY_MAPPING[pod.status.containerStatuses.all { it.ready }].orEmpty()
    )

}