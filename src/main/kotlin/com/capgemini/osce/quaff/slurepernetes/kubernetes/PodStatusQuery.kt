package com.capgemini.osce.quaff.slurepernetes.kubernetes

import io.fabric8.kubernetes.client.DefaultKubernetesClient
import org.springframework.stereotype.Component

@Component
class PodStatusQuery {

    private val client = DefaultKubernetesClient()

    fun podsInNamespace(namespace: String) = client.pods().inNamespace(namespace).list().items

}