package com.capgemini.osce.quaff.slurepernetes

import com.capgemini.osce.quaff.slurepernetes.kubernetes.PodStatusQuery
import com.capgemini.osce.quaff.slurepernetes.kubernetes.StatusExtractor
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
class RestController(val podStatusQuery: PodStatusQuery, val statusExtractor: StatusExtractor) {

    @GetMapping("/{namespace}")
    fun namespaceStatus(@PathVariable namespace: String) =
            Mono.just(namespace)
                    .map(podStatusQuery::podsInNamespace)
                    .flatMapMany { Flux.fromIterable(it) }
                    .map(statusExtractor::toSimpleStatus)
                    .sort { a, b -> a["id"]!!.compareTo(b["id"]!!) }

}