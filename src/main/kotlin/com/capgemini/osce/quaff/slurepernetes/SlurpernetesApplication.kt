package com.capgemini.osce.quaff.slurepernetes

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SlurpernetesApplication

fun main(args: Array<String>) {
    runApplication<SlurpernetesApplication>(*args)
}
